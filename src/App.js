import React from 'react'

import Container from './components/Container'
import Conditions from './components/Conditions'
import ResultTable from './components/ResultTable'

import './App.sass'

class App extends React.Component {
  textOperations = [
    { value: 'Containing' },
    { value: 'Exactly matching' },
    { value: 'Begins with' },
    { value: 'Ends with' },
  ]
  numberOperations = [
    { value: 'Equal' },
    { value: 'Greater than' },
    { value: 'Less than' },
  ]

  constructor(props) {
    super(props)
    this.state = {
      conditions: [
        {
          typeValue: 'text',
          operationValue: 'Containing',
          basicFieldValue: '',
          operations: this.textOperations,
        }
      ],
      resultText: [],
      resultNumber: [],
    }
  }

  handleTypeSelectChange = (e, index) => {
    const value = e.target.value
    const array = this.state.conditions
    if (value === 'text') {
      array[index].typeValue = 'text'
      array[index].operationValue = 'Containing'
      array[index].basicFieldValue = ''
      array[index].operations = this.textOperations
    } else if (value === 'number') {
      array[index].typeValue = 'number'
      array[index].operationValue = 'Equal'
      array[index].basicFieldValue = ''
      array[index].operations = this.numberOperations
    }
    this.setState({
      conditions: array,
      resultText: [],
      resultNumber: [],
    })
  }

  handleValueChange = (e, index, inputName) => {
    const value = e.target.value
    const array = this.state.conditions
    array[index][inputName] = value
    this.setState({
      conditions: array,
      resultText: [],
      resultNumber: [],
    })
  }

  handleClearClick = () => {
    this.setState({
      conditions: [
        {
          typeValue: 'text',
          operationValue: 'Containing',
          basicFieldValue: '',
          operations: this.textOperations,
        }
      ],
      resultText: [],
      resultNumber: [],
    })
  }

  handleAddConditionClick = () => {
    this.setState(prevState => ({
      conditions: [
        ...prevState.conditions,
        {
          typeValue: 'text',
          operationValue: 'Containing',
          basicFieldValue: '',
          operations: this.textOperations,
        },
      ],
      resultText: [],
      resultNumber: [],
    }))
  }

  handleDeleteClick = index => {
    const array = [ ...this.state.conditions ]
    array.splice(index, 1)
    this.setState({ conditions: array })
  }

  onSubmit = e => {
    e.preventDefault()
    const array = this.state.conditions
    const textResult = array.filter(condition => condition.typeValue === 'text')
    const numberResult = array.filter(condition => condition.typeValue === 'number')
    this.setState({ resultText: textResult, resultNumber: numberResult })
  }

  render() {
    const { conditions, resultText, resultNumber } = this.state

    return (
      <Container>
        <form className="Filter__form" onSubmit={this.onSubmit}>
          <Conditions
            conditions={conditions}
            typeSelectChange={this.handleTypeSelectChange}
            valueChange={this.handleValueChange}
            deleteClick={this.handleDeleteClick}
          />

          <div className="Filter__add-condition">
            <button
              type="button"
              disabled={conditions.length === 10}
              title={conditions.length === 10 ? 'Maximum amount reached' : ''}
              onClick={this.handleAddConditionClick}>
              Add condition
            </button>
          </div>

          <button type="submit" className="but but-apply">Apply</button>
          <button type="button" className="but but-clear" onClick={this.handleClearClick}>Clear filter</button>
        </form>

        <div className="Filter__result">
          <ResultTable typeText="Text" results={resultText} />
          <ResultTable typeText="Number" results={resultNumber} />
        </div>
      </Container>
    )
  }
}

export default App