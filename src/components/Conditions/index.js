import React from 'react'
import PropTypes from 'prop-types'

import './style.sass'

const Conditions = ({ conditions, typeSelectChange, valueChange, deleteClick }) => (
  <div className="Conditions">
    {conditions.map((condition, index) => (
      <div className="Conditions__inputs-wrapper" key={index}>
        <div className="Conditions__inputs">

          <div className="SelectInner">
            <select
              name="type"
              value={condition.typeValue}
              onChange={e => typeSelectChange(e, index)}
              required
            >
              <option value="text">Text field</option>
              <option value="number">Number field</option>
            </select>
          </div>

          <div className="SelectInner">
            <select
              name="operations"
              value={condition.operationValue}
              onChange={e => valueChange(e, index, 'operationValue')}
              required
            >
              {condition.operations.map((operation, i) => (
                <option value={operation.value} key={i}>
                  {operation.value}
                </option>
              ))}
            </select>
          </div>

          <input
            name="value"
            type={condition.typeValue}
            value={condition.basicFieldValue}
            onChange={e => valueChange(e, index, 'basicFieldValue')}
          />

        </div>

        {(conditions.length !== 1) && (
          <button type="button" onClick={() => deleteClick(index)}>x</button>
        )}
      </div>
    ))}
  </div>
)

Conditions.propTypes = {
  conditions: PropTypes.array.isRequired,
  typeSelectChange: PropTypes.func.isRequired,
  valueChange: PropTypes.func.isRequired,
  deleteClick: PropTypes.func.isRequired,
}

export default Conditions
