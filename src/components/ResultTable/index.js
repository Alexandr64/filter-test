import React from 'react'
import PropTypes from 'prop-types'

import './style.sass'

const ResultTable = ({ typeText, results }) => (
  <table className="ResultTable">
    <tbody>
      <tr>
        <td>{typeText}</td>
      </tr>
      <tr>
        <td>Operations</td>
        <td>Values</td>
      </tr>
      {results.map((result, index) => (
        <tr key={index}>
          <td>{result.operationValue}</td>
          <td>{result.basicFieldValue}</td>
        </tr>
      ))}
    </tbody>
  </table>
)

ResultTable.propTypes = {
  typeText: PropTypes.string.isRequired,
  results: PropTypes.array.isRequired,
}

export default ResultTable
