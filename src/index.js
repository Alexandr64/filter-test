import React from 'react'
import ReactDOM from 'react-dom'

import App from './App'

import './styling/globalStyle.sass'

ReactDOM.render(
    <App />,
  document.getElementById('root')
)
